import os
import shutil
import zipfile

import pandas as pd
import wget

DOWNLOAD_URL = "https://covid19-static.cdn-apple.com/covid19-mobility-data/2019HotfixDev8/v3/en-us/applemobilitytrends-2020-10-14.csv"
TEMP_PATH = "./tmp/applemobilitytrends-2020-10-14.csv"
FINAL_PATH = "./data/SP_Data_Apple.csv"
REMOVE_TEMP = True

def extract_SP_Apple_data(path_from):
    all_data = pd.read_csv(path_from)
    sp_data = all_data[all_data.region.str.match(r'S[aã]o Paulo')==True]
    return sp_data

def reshape_data(df):
    df = df.reset_index(drop=True)
    info = df[df.columns[:6]]
    data = df[df.columns[6:]]
    final_df = None
    for idx, _ in df.iterrows():
        # Transforma cada linha em uma tabela, onde cada linha representa a medida de um local e data
        row_data = data.iloc[idx].to_frame().reset_index()
        row_data.columns = ['date', 'value']
        row_meta = info.iloc[idx].to_frame().transpose()
        row_meta = pd.concat([row_meta] * len(row_data)).reset_index(drop=True)
        row_to_df = row_data.join(row_meta)

        if final_df is None:
            final_df = row_to_df
        else:
            final_df = pd.concat([final_df, row_to_df])
    final_df = final_df.reset_index(drop=True)
    return final_df

def get_apple_data():
    if not os.path.isdir("./tmp"):
        print("criando directório temporário")
        os.mkdir("./tmp")
    if not os.path.isdir("./data"):
        print("criando directório data")
        os.mkdir("./data")

    if not os.path.isfile(TEMP_PATH):
        print("fazendo download do arquivo csv")
        wget.download(DOWNLOAD_URL, TEMP_PATH)

    print("extraindo dados de São Paulo")
    sp_data = extract_SP_Apple_data(TEMP_PATH)
    sp_data = reshape_data(sp_data)

    print("salvando em", FINAL_PATH)
    sp_data.to_csv(FINAL_PATH)

    if REMOVE_TEMP:
        print("deletando arquivos temporários")
        shutil.rmtree("./tmp")

if not os.path.isfile(FINAL_PATH):
    get_apple_data()
else:
    print("Base de dados já existe em", FINAL_PATH)
