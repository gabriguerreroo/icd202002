import os
import wget
import shutil
import rarfile
import pandas

def get_sivep_data():
    if not os.path.isdir("./tmp"):
        print("criando diretório temporário")
        os.mkdir("./tmp")
    if not os.path.isdir("./data"):
        print("criando diretório data")
        os.mkdir("./data")
    if not os.path.isfile("./tmp/SIVEP.rar"):
        wget.download("http://plataforma.saude.gov.br/coronavirus/dados-abertos/sivep-gripe/INFLUD20-09102020.rar", "./tmp/SIVEP.rar")
    rf = rarfile.RarFile("./tmp/SIVEP.rar", "r")
    rf.extract("INFLUD20-09102020.csv", "./data")
    print("deletando arquivos temporários")
    shutil.rmtree("./tmp")

get_sivep_data()
