from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC
import time

def access_and_extract(driver, nomeHos):
    selects = driver.find_elements_by_tag_name('select')


    for select in selects:
        elm = Select(select)

        if select.get_attribute('ng-model') == 'Estado':
            print('escolhendo o estado')
            elm.select_by_visible_text("SAO PAULO")
        elif select.get_attribute('ng-model') == 'Municipio':
            print('escolhendo a cidade')
            elm.select_by_visible_text("SAO PAULO")

    wait = WebDriverWait(driver, 10)
    pesquisaHos = driver.find_element(By.NAME,'formPesquisa')
    print('pesquisando ', nomeHos)
    form = pesquisaHos.find_element(By.TAG_NAME, 'input')
    button = pesquisaHos.find_element(By.TAG_NAME, 'button')

    form.send_keys(str(nomeHos))
    button.click()
    #wait.until(EC.invisibility_of_element(driver.find_element(By.CLASS_NAME, "row ng-hide")))
    try:
        results = driver.find_element(By.TAG_NAME, 'table')
        tableBody = results.find_element(By.TAG_NAME, 'tbody')
        hospitais = tableBody.find_elements(By.TAG_NAME, 'tr')

        for hospital in hospitais:
            nomeFan = hospital.find_element(By.XPATH, './td[4]')
            if nomeFan.text == nomeHos:
                print("abrindo informações adicionais")

                hospital.find_element(By.XPATH, './td[8]/button').click()
                WebDriverWait(driver, 5).until(lambda d: d.find_element(By.ID, 'dadosBasicosModal').get_attribute('aria-hidden') == 'false')
                modal = driver.find_element(By.ID, 'dadosBasicosModal')
                print('extraindo CEP')
                for campo in modal.find_elements(By.CLASS_NAME, 'form-group'):
                    if campo.find_element(By.TAG_NAME, 'label').text == 'CEP':
                        cep = campo.find_element(By.TAG_NAME,'input').get_attribute('value').replace("-","")
                        break
                break

        driver.find_element(By.CLASS_NAME, "close").click()
    except:
        print("FALHA")
        driver.quit()
        exit()
    finally:
    #driver.quit()
        #print("CEP: ", cep)
        driver.refresh()
        return cep


#get_CEP('HC DA FMUSP HOSPITAL DAS CLINICAS SAO PAULO')
def get_CEP(driver, nomeHos):
    try:
        return access_and_extract(driver, nomeHos)
    except:
        print(nomeHos + " falhou")
        driver.quit()
        exit()
        return

#get_CEP('HC DA FMUSP HOSPITAL DAS CLINICAS SAO PAULO')

def start_driver():
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    driver = webdriver.Chrome(executable_path='./chromedriver', options=chrome_options) # Tira as opções se quiser ver oq tá acontecendo

    driver.implicitly_wait(10)

    print('acessando site')
    driver.get("http://cnes.datasus.gov.br/pages/estabelecimentos/consulta.jsp")
    return driver
