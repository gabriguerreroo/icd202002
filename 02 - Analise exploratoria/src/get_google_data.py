import wget
import os
import zipfile
import shutil
import pandas as pd

def extract_SP_Google_data(pathFrom, pathTo):
    BR_data = pd.read_csv(pathFrom)
    SP_data = BR_data[BR_data.sub_region_1 == "State of São Paulo"]
    SP_data.to_csv(pathTo)
def get_google_data():
    if not os.path.isdir("./tmp"):
        print("criando directório temporário")
        os.mkdir("./tmp")
    if not os.path.isdir("./data"):
        print("criando directório data")
        os.mkdir("./data")

    if not os.path.isfile("./tmp/Region_Mobility_Report_CSVs.zip"):
        print("fazendo download do arquivo zip")
        wget.download("https://www.gstatic.com/covid19/mobility/Region_Mobility_Report_CSVs.zip","./tmp/Region_Mobility_Report_CSVs.zip")
    if not os.path.isfile("./tmp/2020_BR_Region_Mobility_Report.csv"):
        print("extraindo os dados do Brasil")
        with zipfile.ZipFile("./tmp/Region_Mobility_Report_CSVs.zip", "r") as z_ref:
            z_ref.extract("2020_BR_Region_Mobility_Report.csv","./tmp")
    if not os.path.isfile("./data/SP_Data_Google.csv"):
        print("extraindo os dados de São Paulo")
        extract_SP_Google_data("./tmp/2020_BR_Region_Mobility_Report.csv", "./data/SP_Data_Google.csv")
    print("deletando arquivos temporários")
    shutil.rmtree("./tmp")

if not os.path.isfile("./data/2020_BR_Region_Mobility_Report.csv"):
    get_google_data()
