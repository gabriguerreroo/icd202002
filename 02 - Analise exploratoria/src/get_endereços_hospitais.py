import pandas as pd
import Hospitais_cep as H
import os
from geopy.geocoders import Nominatim
import pycep_correios

def get_info(row):
    geolocator = Nominatim(user_agent="test_app")
    endereco = pycep_correios.get_address_from_cep(row['CEP'])
    row['Bairro'] = endereco['bairro']
    location = geolocator.geocode(endereco['logradouro'] + ", " + endereco['cidade'] + " - " + endereco['bairro'])
    if location:
        print(str(row.name)+"/250 tem coordenadas")
        row['Latitude'] = location.latitude
        row['Longitude'] = location.longitude
    else:
        print(str(row.name)+"/250 não tem coordenadas")
    return row
def get_enderecos_hosp():
    if not os.path.isfile('./data/CEP_Unidades.csv'):
        hosps = pd.read_csv('./data/ListaHospitais.csv')
        print(hosps.columns)
        driver = H.start_driver()
        hosps['CEP'] = hosps.apply(lambda row: H.get_CEP(driver, row['Hospitais']), axis=1)
        driver.quit()
        hsps.to_csv("./data/CEP_Unidades.csv", index=False)
    else:
        hosps = pd.read_csv("./data/CEP_Unidades.csv", dtype=str)
    hosps = hosps.apply(get_info, axis=1)
    print(hosps.columns)

    hosps.to_csv("./data/Unidades_geo_info.csv")

    #hosps.to_csv('./data/CEP_Unidades.csv', index=False)

get_enderecos_hosp()
