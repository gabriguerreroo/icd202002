import csv
import os
import sys
from datetime import timedelta

import pandas as pd
import requests

# SUBSTITUIR LOGIN E SENHA PELO EMAIL E SENHA DO QUALAR
LOGIN = ''
PASSWORD = ''

initial_date = '01/01/2019'
final_date = '13/10/2020'

# Os códigos para acessar as estações pelo site do qualar
# Todas as estações da cidade de São Paulo
# As estações comentadas não tem dados em 2019/2020 então não adianta tentar baixar
cetesb_estacoes = {
    #"Cambuci":"90",
    "Capão Redondo":"269",
    #"Centro":"94",
    "Cerqueira César":"91",
    "Cid.Universitária-USP-Ipen":"95",
    "Congonhas":"73",
    "Grajaú-Parelheiros":"98",
    "Ibirapuera":"83",
    "Interlagos":"262",
    "Itaim Paulista":"266",
    "Itaquera":"97",
    #"Lapa":"84",
    "Marg.Tietê-Pte Remédios":"270",
    "Mooca":"85",
    "N.Senhora do Ó":"96",
    "Parque D.Pedro II":"72",
    "Perus":"293",
    "Pico do Jaraguá":"284",
    "Pinheiros":"99",
    #"S.Miguel Paulista":"236",
    "Santana":"63",
    "Santo Amaro":"64"}

# códigos dos parâmetros
cetesb_par = {
    "co":"16",
    "mp10":"12",
    "no":"17",
    "no2":"15",
    "nox":"18",
    "o3":"63",
    "temperature":"25",
    "humidity":"28",
    "wind_velocity":"24",
}

# Para converter os nomes do cetesb em nomes nossos
nome_parametros = {
    'Data': 'date',
    'CO(Monóxido de Carbono) - ppm': 'co',
    'NO(Monóxido de Nitrogênio) - µg/m3': 'no',
    'NO2(Dióxido de Nitrogênio) - µg/m3': 'no2',
    'NOx(Óxidos de Nitrogênio) - ppb': 'nox',
    'MP10(Partículas Inaláveis) - µg/m3': 'mp10',
    'O3(Ozônio) - µg/m3': 'o3',
    'TEMP(Temperatura do Ar) - °C': 'temperature',
    'UR(Umidade Relativa do Ar) - %': 'humidity',
    'VV(Velocidade do Vento) - m/s': 'wind_velocity'
}

def get_CETESB_data(data_inicial, data_final, estacoes, parametros):
    if not os.path.isdir("./data"):
        print("criando directório data")
        os.mkdir("./data")
    if not os.path.isdir('./data/cetesb'):
        print("criando directório data/cetesb")
        os.mkdir("./data/cetesb")

    # iniciando sessão para guardar cookies
    session, at = authenticate_cetesb()

    print("auth response:", at.status_code)
    print("")

    # acessando os dados para cada estação da cidade de são paulo
    for estacao in estacoes:
        dados_est = None
        print("acessando dados da estação: ", estacao)
        for param_name in parametros:
            csv_param = get_dados_parametro(session,
                                            data_inicial,
                                            data_final,
                                            estacoes[estacao],
                                            parametros[param_name])
            #checa se recebeu um arquivo csv
            if csv_param:
                # Transforma o csv em dataframe
                df_param = param_csv_to_df(csv_param)

                # Ajusta o campo de data-hora
                df_param.Data = pd.to_datetime(df_param.Data)
                df_param = df_param.apply(change_time, axis=1)
                df_param.Data = pd.to_datetime(df_param.Data.dt.strftime('%d/%m/%Y') + " " + df_param.Hora)
                df_param.drop(['Hora'],axis=1,inplace=True)
                df_param.set_index('Data',inplace=True)

                # Coloca a nova coluna no dataframe
                if dados_est is None:
                    dados_est = df_param.copy()
                else:
                    dados_est = dados_est.join(df_param,how='outer')
                print(param_name, ": OK")
            else:
                print(param_name, ": Não tem")

        if dados_est is not None:
            # Troca o nome das colunas
            old_names = dados_est.columns.values.tolist()
            new_names = [nome_parametros[old_name] for old_name in old_names]
            dados_est.columns = new_names

            # Ajusta os campos que são valores com virgula
            for column in dados_est.columns:
                if column in ('temperature', 'wind_velocity', 'co'):
                    dados_est[column] = dados_est[column].apply(number_string_to_float)

            print("salvando dados")
            dados_est.to_csv("./data/cetesb/" + estacao + "-cetesb.csv")
        else:
            print("estação não tinha nenhum dado para esse periodo")
        print("")

def authenticate_cetesb():
    session = requests.Session()
    homeR = session.get("https://qualar.cetesb.sp.gov.br/qualar/home.do")
    print("home login response:", homeR.status_code)

    # autenticando com o qualar
    at = session.post(
        "https://qualar.cetesb.sp.gov.br/qualar/autenticador",
        data = {"cetesb_login" : LOGIN,
                "cetesb_password" : PASSWORD,
                "enviar" : 'OK'},
        headers = {"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"}
    )
    return session, at

def get_dados_parametro(sessao, data_inicial, data_final, estacao, parametro):
    r = sessao.post("https://qualar.cetesb.sp.gov.br/qualar/exportaDadosAvanc.do?method=exportar" ,
    data = {
    "dataInicialStr":data_inicial,
    "dataFinalStr":data_final,
    "estacaoVO.nestcaMonto":estacao,
    "nparmtsSelecionados": parametro}
    )
    if r.status_code != 200:
        print("erro ao baixar: status code =", r.status_code)
    try:
        r.headers['content-type']
    except KeyError:
        print("autenticação falhou. verificar usuario e senha")
        sys.exit()

    #checa se recebeu um arquivo csv
    if r.headers['content-type'] == "text/csv;charset=CP1252":
        return r.content.decode(r.encoding,errors="replace")
    else:
        return None

def change_time(x):
    if x.Hora == "24:00":
        x.Hora = "00:00"
        x.Data = x.Data+timedelta(days=1)
    return x

def param_csv_to_df(csv_data):
    """Transforma o csv de um parametro de uma estação em um DataFrame."""
    cr = csv.reader(csv_data.splitlines(), delimiter=';')
    csv_list = list(cr)
    csv_list = csv_list[6:]
    csv_list[0][2] = csv_list[1][2]
    csv_list.pop(1)
    return pd.DataFrame(csv_list[1:], columns=csv_list[0])

def number_string_to_float(num_str):
    if num_str:
        return float(num_str.replace(',', '.'))
    else:
        return None


if '' not in (LOGIN, PASSWORD):
    get_CETESB_data(initial_date, final_date, cetesb_estacoes, cetesb_par)
else:
    print('Insira seu usuario e senha do sistema QUALAR do CETESB no inicio do script!')
