from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC
import time
import pandas as pd

LOGIN = ''
PASSWORD = ''

def get_coord(driver):
    wait = WebDriverWait(driver, 10)

    driver.get("https://qualar.cetesb.sp.gov.br/qualar/home.do")
    driver.find_element(By.NAME, "cetesb_login").send_keys(LOGIN)
    driver.find_element(By.NAME, "cetesb_password").send_keys(PASSWORD)
    driver.find_element(By.NAME, "enviar").click()

    driver.get("https://qualar.cetesb.sp.gov.br/qualar/relConfiguracaoEstacao.do?method=pesquisarInit")
    opc = Select(driver.find_element(By.NAME, "municipioVO.cmuncpEct"))
    opc.select_by_visible_text("São Paulo")

    original_window = driver.current_window_handle
    driver.find_element(By.NAME, "btnGerar").click()
    wait.until(EC.number_of_windows_to_be(2))
    for window_handle in driver.window_handles:
        if window_handle != original_window:
            driver.switch_to.window(window_handle)
            break

    wait.until(EC.title_is("Serviços - CETESB"))
    cetesb = pd.DataFrame()
    table = driver.find_element(By.XPATH, "/html/body/table[2]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody")
    for i in range(3, 25):
        line = table.find_elements(By.XPATH, f"./tr[{i}]/td")
        cetesb = cetesb.append({'Estação':line[0].text, 'Latitude':line[21].text, 'Longitude':line[23].text}, ignore_index=True)

    cetesb.to_csv("./data/CETESB_geo_info.csv", index=False)

try:
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    driver = webdriver.Chrome(executable_path='./chromedriver', options=chrome_options) # Tira as opções se quiser ver oq tá acontecendo
    get_coord(driver)
except:
    raise
finally:
    driver.quit()
