# Projeto: Impacto da quarentena nos níveis de poluição

## Equipe: Fumaceiros

## Descrição:

Com a pandemia de COVID-19, houveram várias ações promovendo quarentena e diminuição das atividades presenciais por todo o país. Nós hipotetizamos que devido a isso diversos efeitos que causam poluição do ar nas cidades, como trânsito de veículos e atividade industrial, tenham diminuído no período de atividade reduzida. Isso levaria, então, a uma diminuição nos níveis de poluição e a uma melhora da qualidade do ar nas cidades em que houve quarentena.

Pretendemos analisar índices de qualidade do ar em algumas regiões ao longo do período de quarentena, controlando com índices de atividade, observando se houve uma diminuição significativa e se existe correlação entre o nível de poluição e o nível de atividade.

## Membros senior:

Nicolas Abril, 1367366, nicolasabril, UTFPR

Gabriel Carrico Guerrero, 1860216, gabriguerreroo, UTFPR

Matheus Vinicius Barcaro Turatti, 1570609, ander9999, UTFPR

## Membros junior:

João Guilherme Martins Silva, 2088878, JGMartins, UTFPR